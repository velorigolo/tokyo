#+TITLE: Jour 103 
#+DATE: 2018-10-16T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day103.jpg
#+PLACE: Kegen
#+SUMMARY: Les 7 plaies du vélo
#+MAP: true

** Etape 

Karakol - Kegen

** Kilomètres

114

** Météo

FROID

** Phrase du jour

Je ne comprends pas pourquoi les gens font du vélo dans cette zone - Quentin

** Résumé

Les 7 plaies de Mathieu et Quentin.

L'étape d'aujourd'hui a été dure, facilement dans notre top 5 du
voyage. Elle a réussi l'exploit de combiner la quasi totalité des
pires choses pouvant arriver en vélo. Je vous présente donc les 7
plaies des cyclistes en voyage.

*** Le chemin de terre dégueulasse 

   La plus basique des plaies mais également une des pires. Quand le
   bon asphalte laisse place a un chemin dégueulasse, avec des cailloux
   et parfois même des vaguelettes (sortes de micro reliefs répétés),
   tu sais que tu vas souffrir. Réduit la vitesse de croisière très
   fortement et cause des douleurs au dos. Aujourd'hui c'est 90% de
   chemin horrible que nous avons eu. Google map nous a bien eu.

*** La température

   Il est facile de l'oublier mais l'être humain est vraiment nul pour
   gérer les extrêmes de température quand il fait du vélo. Trop chaud,
   l'étape va être pesante et longue et tu vas mal dormir. Trop froid,
   tu vas souffrir de partout, ne plus sentir tes doigts et avoir pour
   seule envie de te réfugier au chaud. Aujourd'hui, nous avons passé
   un col a 2200 mètres ou le vent glacé dans notre gueule devait être
   pas loin de -5 degrés. Pour preuve, notre eau a gelé dans les
   bouteilles donc bon.

*** Le dénivelé

   En vélo, le plat n'existe pas vraiment, le faux plat
   est partout. Mais quand ça monte véner, ça monte véner et tu te
   retrouves a forcer sur le petit plateau en avançant super
   lentement. Les kilomètres passent pas, tu t'énerve tout seul et tu
   attends la descente tout en sachant qu'il y'aura une autre montée
   derrière. Aujourd'hui, gros dénivelé avec col a 2200 mètres au point
   que nous avons du pousser les vélos sur 2 kilomètres tellement
   c'était la mort.

*** L'isolement 

   L'avantage de base en vélo c'est que tu n'es jamais
   vraiment exposé, il y'a toujours des voitures qui passent et des
   baraques au bord de la route. Mais parfois tu te retrouves dans un
   endroit complètement perdu au milieu de nulle part, sans aucune
   échappatoire : tu continues ou tu t'arrêtes et tu pries pour qu'une
   voiture passe. Aujourd'hui, plateau montagneux complètement vide
   avec une voiture toutes les heures et aucun endroit ou se poser au
   chaud.

*** Les petits tracas divers 

   Un peu a part mais toujours présent, il
   s'agit des inclassables. La bouteille d'eau qui tombe, se tromper de
   route et devoir revenir en arrière, galérer pour trouver de quoi
   manger ou dormir, la liste est infinie. Aujourd'hui la thématique
   était de faire tomber des trucs a cause des vibrations du chemin. Ma
   sacoche arrière gauche est tombée 5 fois, dont 2 fois dans une pente
   raide, et Mathieu a fait tomber 4 fois sa bouteille d'eau, le
   forçant a s'arrêter. Nous avons également du faire deux demi tours
   en nous trompant de chemin.

*** Les casses mécaniques 

   Forcément un vélo ça peut casser a force de
   prendre sur soi, que ce soit la chaine qui rompt, la classique
   crevaison ou pire, le dérailleur qui claque. Tout ça prend du temps
   a réparer, casse ton rythme et t'énerve, quand tu n'est pas obligé
   d'attendre de l'aide parce que tu n'as pas de quoi réparer. C'est la
   seule plaie dont nous avons été épargné aujourd'hui, et
   heureusement.

*** Le vent de face

   La plaie principale. Elle seule dirige toutes les
   autres plaies. Tout est supportable, même cumulé, sans vent de
   face. Au contraire, une étape parfaite sera très facilement gachée
   par le vent de face. Il rend toute communication impossible,
   ralentit de plus de 10 km/h le rythme de base et provoque une très
   grande fatigue car il est difficile de contrôler le vélo quand le
   vent te ballote dans tous les sens. Aujourd'hui, le vent de face a
   été de la partie sur l'entièreté de l'étape et s'est cumulé au reste
   pour faire un bon gros tas de caca.

 Comme vous pouvez le constater, on a bien accumulé aujourd'hui et on
 est bien séché, il est l'heure d'aller dormir. Le vélo c'est vraiment
 pas du tout rigolo parfois.
