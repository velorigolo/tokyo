#+TITLE: Jour 11
#+DATE: 2018-07-15T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day11.jpg
#+PLACE: Scharding
#+SUMMARY: Les commentateurs autrichiens
#+MAP: true

** Etape 

Binabiburg - Scharding 

** Kilomètres

92

** Météo

Très chaud

** Phrase du jour

"Au moins ici on a pas de klaxons" - Quentin

** Résumé

Etape très roulante à part le moment ou Mathieu a foncé dans un champ
de mais. Le combo de sa fatigue allié à ma douleur au genou et
vaguement le match de finale de coupe du Monde nous décide à une
petite étape. Nous galérons à trouver de quoi manger en ce dimanche
avant de tomber sur une providentielle station service moche qui nous
permet de boire un peu de frais. Pour la bonne nouvelle, nous sommes
enfin en Autriche! Fini les allemands fermés et policés! (ah on me dit
dans l'oreillette que non, c'est la même chose). Ca fait trois pays de
passés, il en reste 10!

Arrivés a l'hotel, Mathieu mets le match pendant que je vais au sauna
pour transpirer tel un énorme bourgeois qui s'en fiche du foot. Je
reviens à temps pour voir la victoire annoncée par des commentateurs
autrichiens pas du tout motivés. Ca doit plus hurler sur TF1.GG la
France, et gros bisoux!
