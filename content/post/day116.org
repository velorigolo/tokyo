#+TITLE: Jour 116 
#+DATE: 2018-10-29T00:17:43Z
#+DRAFT: false
#+AUTHOR: Mathieu
#+COVER: day116.jpg
#+PLACE: Fukuyama
#+SUMMARY: Unités alternatives
#+MAP: true

** Etape 

Hiroshima - Fukuyama

** Kilomètres

103

** Météo

Parfaite again

** Phrase du jour

La photographie c'est comme les hamburgers j'ai du mal à distinguer
le moyen du bon - Mathieu

** Résumé

Après une visite émouvante de la ville d'Hiroshima hier, nous
redémarrons tranquillement aujourd'hui. La sortie de la ville se fait
sans encombres et nous rejoignons rapidement la campagne japonaise et
ses montagnes.

Je sais que nous devons franchir un col et guette anxieusement les
alentours. Je ne tarde pas à apercevoir sur ma gauche, une ville
perchée à flanc de montagne. Cette ville est traversée par une sorte
de viaduc des enfers dont la pente me parait délirante.

Il faut bien se dire que nous sommes au pays où les travaux sont
signalés par des petits plots en forme de lapin qui se tiennent la
main, les voitures sont des sortes de papa-mobiles rabougries et les
feux tricolores jouent de bien étranges mélodies.

J'ai donc assez peur de ce viaduc et me dis qu'il s'agit peut-être
d'une mise à l'épreuve de la foi des japonais, où seul le preux pourra
gravir la pente à 35% sans noyer son moteur.

En approchant nous réalisons avec soulagement qu'il s'agit en fait
d'un train à crémaillère vraiment impressionnant ! La route pour
voiture est beaucoup plus traditionnelle et nous l'avalons au grand
plateau.

A l'heure du déjeuner nous avons déjà effectué le gros de
l'étape. Dans l'après midi, Quentin essaie de me prouver sa maîtrise
des unités mais confond allègrement les miles terrestres et nautiques
et les lieux. Je me fais un malin plaisir à lui prouver ses torts à
l'aide de ma précieuse 3G.

Je décide par ailleurs d'employer des unités délaissées et qui
mériteraient plus de soutien, comme le picomètre pour les distances et
le femtomètre par téra secondes pour la vitesse. De bonnes sessions de
calcul mental en perspective.

Sentant qu'il est peu réceptif, je l'achève en lui expliquant
l'histoire du dernier théorème de Fermat et de la conjecture
d'Euler. Ça vaut bien pour toutes les fois où il me parle de Pokémons.

Nous arrivons vers 16h30 à Fukuyama, une autre étape rondement menée.
