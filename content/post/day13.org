#+TITLE: Jour 13 
#+DATE: 2018-07-17T10:17:43Z
#+DRAFT: false
#+AUTHOR: Mathieu
#+COVER: day13.jpg
#+PLACE: Vienne
#+SUMMARY: Le maillon de l'enfer
#+MAP: true

** Etape 

Enns - Vienna

** Kilomètres

122

** Météo
   
Pluvieuse

** Phrase du jour
   
C'est pas le bon chemin mais ça rejoint - Mathieu

** Résumé

Nous émergeons vers 8h30 dans notre motel pour routier. Le réveil n'a
pas sonné ! Mon montage éléctrique me permettant de recharger trois
appareils en les dérivant sur la batterie externe a échoué. Nous
allons devoir utiliser la dynamo pour recharger le téléphone portable,
sans quoi nous serons sans guidage !

Le petit déjeuner est à la hauteur de la propreté de l'hotel. Nous ne
nous attardons pas et sommes rapidement sur la route. Nous longeons le
Danube jusqu'à la pause déjeuner. Le temps est orageux mais nous
évitons les averses que l'on aperçoit au loin.

De retour sur les vélos, il nous faut traverser le Danube pour
poursuivre sur la rive sud. Nous attendons le bac en vain pendant 20
minutes et décidons d'emprunter le prochain pont. Arrivés à Melk nous
sommes finalement rattrapés par l'orage et nous faisons doucher.

Quentin profite de l'occasion pour enfiler ses sur-chaussures
étanches. L'efficacité et l'esthétisme sont douteuses mais je sais
qu'il le fait exprès pour m'énerver car j'ai renvoyé les miennes par
la poste.

Nous poursuivons sous la pluie lorsque dans une cote ma chaine se
bloque. Quentin lancé dans son élan parvient au sommet de la cote et
disparait de ma vue. Je m'installe au fond du bas-coté boueux pour
étudier la situation. Après plusieurs minutes, j'identifie le
problème, un maillon de ma chaine s'est tordu !

Je dérive la chaine et installe un maillon rapide. Il faut ensuite
remonter le vélo sur 1 mètre d'une pente boueuse pendant que les
camions autrichiens me frolent. Heureusement ma réparation fonctionne,
je peux repartir et rejoindre Quentin au sommet de la cote.

Nous parvenons à St-Polten où nous prenons un train pour s'éviter la
traversée de la banlieue Viennoise. Il nous reste encore quelques
kilomètres avant de nous poser dans notre Airbnb. Le repos sera bien
mérité !
