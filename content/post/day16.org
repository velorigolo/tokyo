#+TITLE: Jour 16 
#+DATE: 2018-07-20T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day16.jpg
#+PLACE: Mosonmagyarovar
#+SUMMARY: Piste Cyclable
#+MAP: true

** Etape 

Vienna - Mosonmagyarovar

** Kilomètres
   
107

** Météo

Très très chaud!

** Phrase du jour

"Putain je recommence à crisser" Quentin

** Résumé

Une journée très sympathique sous le signe des pistes cyclables. En effet Mathieu a décidé que comme tous les vieux qui font l'euroroute, il fallait absolument passer par les pistes cyclables quitte à se rallonger de 20 kilomètres. Pendant notre pique nique sur la piste cyclable dans un relais pour cyclotouristes, nous nous rendons compte  que la piste cyclable que nous avons emprunté, rallongée par une déviation cyclable pour cause de travaux, nous avait mis 20 kilomètres dans le lard. Pour atteindre la ville que nous voulions dans la journée, il faudrait emprunter des routes, et c'est hors de question. Les cyclables sont bien plus belles, on y croise des cyclotouristes (moyenne d'âge basse 65 ans) et il n'y a pas de voitures. Et puis comme on dit, plus c'est long plus c'est bon (cela vaut évidemment pour les pistes cyclables également).

Nous arrivons donc, par une piste cyclable vous vous doutez bien, à la jolie ville au nom imprononçable de Mosonmagyarovar, qui veut dire "on kiffe le vélo" en hongrois. Quelle heureuse coincidence!

Au final l'étape du jour roulait bien, c'est normal c'était une piste cyclable, radweg en allemand, je sais pas comment on dit en hongrois ou en slovaque. Demain Mathieu voulait prendre une piste cyclable rajoutant encore 10 kilomètres à l'étape initiale, mais j'ai réussi à sauver un peu de route pour regagner un peu de bornes. (mais on fera quand même deux bons tiers de pistes cyclables)

Je pense à fonder la SPRCCFC, la société de protection des routes contre les cyclotouristes fan de cyclables. Vous pouvez faire un don à l'association via ma page paypal Fildrong. N'hésitez pas, sauver les routes c'est important.
