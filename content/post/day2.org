#+TITLE: Jour 2
#+DATE: 2018-07-06T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day2.jpg
#+PLACE: Bar sur Aube
#+SUMMARY: Le décapitator
#+MAP: true

** Etape 

Nogent Sur Seine - Bar Sur Aube

** Kilomètres

118

** Météo

Il fait chaud mais ça passe (ça tape fort par contre)

** Phrase du jour

"Biiiiite" - Mathieu

** Résumé

Départ à 10h30 tranquilles, pédalage bien rapide jusqu'à Troyes. Petit
pique nique dans un parc ou Mathieu décide de sortir le drone et de
faire quelques tests. 17 secondes plus tard, ce dernier me rentre
dedans et va se crasher dans le banc, brisant au passage 3 hélices et
m'esquintant le bras. Puis il se rend compte qu'il a pété l'embout du
tube de crème solaire. Super. Fin d'étape très jolie mais vent de face
relou, Mathieu perd son rétroviseur qui se détache 2 fois avant qu'il
ragequit en le foutant dans les sacoches. 200 mètres plus loin il se
rend compte qu'en tendant trop le cable du compteur kilomètrique il a
conduit le cable à se sectionner. On le répare au scotch et à la pince
coupante et il remarche. Mathieu a donc aujourd'hui cassé un tube de
crème solaire, des hélices de drone, un rétroviseur et son deuxième
compteur (oui il a aussi coupé le cable du premier par inadvertance
pendant la préparation). Ca fait deux jours qu'on est partis j'ai
confiance pour la suite (bof en fait). Sinon on est contents d'avoir
fait pratiquement 120 km dès le deuxième jour!


