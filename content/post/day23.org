#+TITLE: Jour 23 
#+DATE: 2018-07-27T10:17:43Z
#+DRAFT: false
#+AUTHOR: Mathieu
#+COVER: day23.jpg
#+PLACE: Belgrade
#+SUMMARY: No ticket
#+MAP: true

** Etape 

Novi Sad - Belgrade

** Kilomètres

57

** Météo

Orageux

** Phrase du jour

This is Serbia here, no ticket ! - L'agent de la gare à qui on essaie
d'acheter des billets.

** Résumé

Départ difficile, la nuit a été mauvaise en raison de la chaleur. Nous
arrivons assez vite sur la grosse difficulté du jour. Un col de
quelques kilomètres sur un route en chantier.

Ignorant la peur car sa tendinite ne le fait plus souffrir Quentin
opte pour une montée du col au troisième plateau. Pensant que les 9%
de pente et le slalom entre les bulldozers suffiront à le décourager
je le suis sans trop m'inquiéter.

Hélas il ne renonce pas et je me vois contraint d'avaler cette section
à vive allure pour le suivre. Nous redescendons ensuite vers le Danube
et rejoignons sans trop de difficulté la banlieue de Belgrade. Là,
nous devons prendre une train pour s'éviter un nouveau moment de
bonheur sur les 4x4 voies Serbes.

Nous tentons d'acheter des billets et déclenchons l'hilarité
générale. Selon l'agent de la gare, en Serbie, "No ticket". Il ne nous
reste donc plus qu'à escalader le marchepied de 1m50 pour hisser les
vélos dans le train.
