#+TITLE: Jour 3
#+DATE: 2018-07-07T10:17:43Z
#+DRAFT: false
#+AUTHOR: Mathieu
#+COVER: day3.jpg
#+PLACE: Bourbonne les bains
#+SUMMARY: Le dénivelé qui tue
#+MAP: true

** Etape 

Bar Sur Aube - Bourbonne les bains

** Kilomètres

102

** Météo

CHALEUR

** Phrase du jour

Est-ce que tu es en train de frotter tes pieds qui puent sur mon
appareil photo à 700 balles ? - Quentin

** Résumé

Départ tardif sous le cagnard, et montées incessantes, les vélos de
40kg rendent s'avèrent plus difficile à trainer que prévu. L'arrivée à
Chaumont après 400 mètres de dénivelé est salvatrice.  Nous nous
installons dans une ruelle pour profiter d'une plaque de ventilation
et déguster un chaussée aux moines.

Reprise vers 14h, nous découvrons que Google Map classe les routes non
carossées avec des pentes à 12 pourcent dans la catégorie "vélo". Il
faut pousser les vélos. De retour sur la départementale, les montées -
descentes se poursuivent.  Je décide de brancher Nostalgie pour faire
profiter Quentin de mon interprétation de "Hey Jude".  Cela lui donne
un coup de boost et l'encourage à avancer pour ne plus m'entendre. Je
retiens la technique.

Bilan 900 métres de dénivelé, pareil pour demain, j'envisage de
m'alléger de tout mon poids et d'abandonner Quentin qui dit que je
chante mal.

