#+TITLE: Jour 4
#+DATE: 2018-07-08T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day4.jpg
#+PLACE: Belfort - Chez Alex
#+SUMMARY: Faux espoirs
#+MAP: true

** Etape 

Bourbonne les bains - Belfort

** Kilomètres

110

** Météo

Encore chaud mais chaud

** Phrase du jour

"J'ai une nouvelle paire de fesses qui a poussé tellement c'est enflé" - Quentin


** Résumé

Départ plus tot ce matin, et on a bien fait, ça a commencé très fort avec des petits chemins qui montent à n'en pas finir (920 mètres de dénivelé positif bordel). Google map se sentant intelligent, il nous fait passer par des petits bois très bucoliques mais rapidement chiants, la montée avec des vélos de 40 kilos c'est déjà chaud mais alors sans goudron c'est la mort. On s'est arrêté dans le seul restaurant ouvert a 20 kilomètres à la ronde et un abonné nous a reconnu (gros bisou à toi!). Mathieu mange trop de pâtes et a mal au ventre après. Fin d'étape avec des gens qui se croient intelligents à nous dire "tkt mec ça descend jusqu'à Belfort" alors que PAS DU TOUT PUTAIN DE MERDE CA MONTE TOUT LE TEMPS TA MERE LE FAUX ESPOIR. Alex le très gentil abonné et Youtuber (mets un pouce bleu) nous accueille dans son joli chez soi après nous avoir escorté en voiture. Pas de photos désolé; on en a trop chié pour s'arrêter (go google image "paysage Vosges" au pire).
