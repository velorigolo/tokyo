#+TITLE: Jour 43 
#+DATE: 2018-08-16T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day43.jpg
#+PLACE: Chaschuri
#+SUMMARY: C'était dur putain.
#+MAP: true

** Etape 

Zestaponi - Chaschuri

** Kilomètres

50

** Météo

On souffrait trop pour la ressentir

** Phrase du jour

"C'était dur putain" - Les deux

** Résumé

C'était dur putain.

Nous partons de notre hotel en pensant savoir ce qui nous attend : 40
kilomètres de montée de col avec 1000 mètres de dénivelé. Pas super
facile mais gérable. Béni soit les ignorants. Le premier kilomètre
donne directement le ton : le vent descendant la vallée nous assaille
et nous brimquebale dans tous les sens, avec des bouffées surement pas
loin de 100 kilomètres par heure. C'était dur putain. Mais ce n'est
pas le pire.

Le pire c'est que cette route, pourtant indiquée en jaune sur toutes
les cartes, ce qui veut dire "tkt mec c'est une route bien foutue",
est une sorte de chemin dégueulasse. Et ce, sur 40 kilomètres. C'était
dur putain.

Avec le vent de face.  Et le dénivelé permanent.

En fait c'est genre une super randonnée en VTT mais à la place des VTT
tu mets un vélo qui pèse 40 kilos. C'était dur putain.

On a fait tous les chemins de terre possibles : sable, gravier, boue,
terre avec des trous, gros cailloux. C'était dur putain.

Au bout d'une heure, nous avons fait 11 kilomètres. Au bout de
deux, 17. Au bout de trois, 23. C'était dur putain.

Bref nous sortons de ce passage a 16h30, après 5h de pédalage. Un
petit saut sur le goudron retrouvé et nous débarquons à Chaschuri,
heureusement la guest house est super sympa. La Géorgie nous
promettait de la montagne, on a été servis. Demain l'étape devrait
être plus classique et on arrivera à Tbilissi avec une journée de
repos à la clé. C'était dur putain.

Ah et je vous ai dit que j'ai crevé? C'était dur putain.

