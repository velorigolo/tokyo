#+TITLE: Jour 57 
#+DATE: 2018-08-31T10:17:43Z
#+DRAFT: false
#+AUTHOR: Mathieu
#+COVER: day57.jpg
#+PLACE: Au bord d'une route avant Zhetybay
#+SUMMARY: Viva el desierto
#+MAP: true

** Etape 

Aktau - Zhetybay

** Kilomètres

83

** Météo

Chaud chaud chaud

** Résumé

Nous sommes de retour, cette fois ci au Kazakhstan, reposés et guéris
de la turista par une bonne pause à Baku.

Aujourd'hui petite étape pépère, pas de vent de face, il fait super
bon, le désert est ultra accueillant. On pédale pendant 7h mais c'est
juste parce que c'est super beau. La route est belle et sans dénivelé.

Non en fait c'est tout le contraire, mais j'en ai marre de faire des
billets racontant notre souffrance. En vrai il y a tellement de vent
que l'on garde à peine l'équilibre sur le vélo.

Cette fois ci, c'est le désert puissance maximale. Nous passons à 120
mètres sous le niveau de la mer et ne croisons absolument rien sur
presque 50 kilomètres à part un chameau.

Le paysage est magnifique mais Quentin craque complètement. Il me
confie avoir atteint les limites de ce dont il est humainement capable
et veut finir le voyage en train. Je sais qu'il se plaint toujours
beaucoup mais suis pour une fois tenté de le croire.

Nous nous arrêtons au bord de la route et sommes hébergés par le
gérant de la supérette. Demain, suite des hostilités, espérons que le
vent ait tourné d'ici là !
