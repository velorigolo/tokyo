#+TITLE: Jour 62 
#+DATE: 2018-09-08T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day62.jpg
#+PLACE: Noukous
#+SUMMARY: Train de nuit
#+MAP: true

** Etape 

Beineu - Noukous

** Résumé

Après le désert qui nous a fortement taxé, physiquement comme
mentalement, nous nous retrouvons à Beineou. encore 400 kilomètres de
désert à faire pour arriver dans l'oasis ouzbèque. Et ça, c'est trop
relou. En plus de ça Mathieu tombe malade (type fièvre). Le même jour
je me rechope une tourista (et en plus y'a des millions de moustiques
même si ils ont rien a foutre dans un désert).

IL EST TEMPS DE SORTIR DE CE LIEU DE MORT

Nous décidons de prendre le train pour Noukous. Mais ce n'est pas si
facile. Il faut d'abord prendre le billet sur Internet et surtout
aller récupérer son billet à la gare. Comme Mathieu est au quatrième
dessus je me dévoue héroïquement. Une fois sur place les gens sont
très intrigués par un mec à la peau claire et qui fait presque 2
mètres. Au milieu de cette moyenne d'1m70 je me rend compte que je
détonne. On m'interroge et me spamme, et on essaie de m'arnaquer ma
place à plusieurs reprises. Je ne me laisse pas faire en bon français
chiant qui colle a la personne devant, non mais! Après quelques
galères j'obtiens le billet et nous sommes prêts à partir.

Nous sortons donc de notre hôtel a 2h du matin pour aller prendre le
train de 3h, frais comme des poissons morts de la semaine dernière. Le
chef de wagon est sympa et nous attribue des places près des vélos
(après s'être foutu de ma gueule quand j'essayais de les monter dans
le wagon situé approximativement 5 mètres au dessus du sol). Nous nous
écrasons sur les couchettes et dormons.

Le matin c'est marrant, une dizaine de personnes traverse le wagon
pour proposer plein de trucs qu'ils ont cuisiné. Et tant que tu n'as
rien acheté ils repasseront, on dirait une pub sur Internet! Ils se
connaissent tous et se foutent sous nos couchettes pour parler entre
eux. Et plus tu cries fort sous les touristes qui se réveillent plus
t'es fort. Nous achetons 2 samsa, du plov, des pelmeni et un gros pain
super bon, histoire de remettre un peu de tourista dans nos coeurs
(enfin nos intestins). Le train arrive après 14h de transport - soit
environ du 30 à l'heure.

Maintenant que nous sommes à Noukous, nous allons prendre quelques
"vacances" du vélo pour en profiter pour visiter le coin car nous
voulons voir la ville de Khiva et la mer d'Aral. Le problème à vélo
c'est qu'il est difficile de partir loin de son itinéraire, donc louer
une voiture semble le meilleur choix.

Des vacances dans les vacances quoi :)

Ah et n'oubliez pas de regarder le dernier vlog !

{{< ytvideo >}}
{{< youtube id="2ldfyS9sw78" >}}
{{< /ytvideo >}}
