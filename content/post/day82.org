#+TITLE: Jour 82 
#+DATE: 2018-09-25T10:17:43Z
#+DRAFT: false
#+AUTHOR: Mathieu
#+COVER: day82.jpg
#+PLACE: Tachkent
#+SUMMARY: Sifflements
#+MAP: true

** Etape 

Goulistan - Tachkent

** Kilomètres

125

** Météo

Comme hier

** Phrase du jour

Я не собака (je ne suis pas un chien) - Quentin

** Résumé

Hier soir, petit tour de la ville avec des étudiantes Ouzbèkes à qui
j'ai donné une interview. Elles nous emmènent manger une pizza et nous
leur chantons du Francis Cabrel pour les remercier.

Quentin tente un débat sur la théologie, malheureusement l'assistance
semble un peu larguée. Une des étudiantes trouve Quentin "handsome" et
il est content. Elles nous forcent à prendre 300 selfies et nous
partons ensuite nous coucher.

Comme les deux jours précédents, ça roule bien et le temps est
agréable. Nous en profitons pour bourriner un peu et avons déjà
parcouru 83 kilomètres à l'heure de la pause.

Dans l'après midi Quentin attend désespérément qu'on le siffle à
nouveau pour pouvoir ressortir la phrase en Russe que je l'entends
répéter toutes les 5 minutes pour ne pas l'oublier (voir la phrase du
jour).

Pour ma part, je trouve que c'est un peu dangereux et mets en place
une technique de fuite rapide au cas ou. C'est de toute façon quelque
chose que je rêvais d'essayer depuis nos galères dans le
désert. Je repère un camion qui roule un peu plus vite que nous et m'y
accroche par l'arrière.

Je trouve la sensation de vitesse et d'économie d'énergie assez
grisante et ne lâche plus la remorque. Le chauffeur ne trouve en
revanche cela pas à son goût et commence à raser le bas côté et
klaxonnant. Ce doit être un signe, je me décroche et constate que
Quentin est très loin derrière. Cette technique de fuite efficace est
donc validée.

Malheureusement pour l'un et heureusement pour l'autre, personne ne
nous siffle. Nous arrivons rapidement à Tachkent et traversons la
ville beaucoup plus rapidement que prévu. Nous voici installé dans un
petit appartement pour la prochaine semaine en attendant mes parents
qui terminent leur escapade Ouzbèke par cette ville. Encore un peu de
repos en perspective, c'est parfait !
