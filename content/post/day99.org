#+TITLE: Jour 99 
#+DATE: 2018-10-12T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day99.jpg
#+PLACE: Ananyevo
#+SUMMARY: C'est beau
#+MAP: true

** Etape 

Tamchy - Ananyevo

** Kilomètres

91

** Météo

Il fait frisquet

** Résumé

Ce matin Mathieu est bon prince, il me promet un réveil tardif (car il
fait trop froid a 9h). Il est bien dommage que la propriétaire de la
maison ou nous avons dormi décide un réveil a 7h30 pour le petit
déjeuner.

Départ donc sous un temps incertain mais certainement la palme du plus
beau paysage du voyage. A gauche des montagnes de plus de 4000 mètres
d'altitude. A droite le lac Issik Koul, deuxième plus grand lac de
montagne du monde (on voit même pas la rive d'en face). Nous avançons
moins vite que les derniers jours mais le rythme reste bon.

A midi petite pause ou nous trouvons du bon pain chaud. Nous le
dégustons sur des marches mais nous avons fait une erreur de débutant
en nous mettant a côté de la sortie de l'école locale, et c'est la fin
des cours. Nous alternons ainsi bouchée de pain au fromage et sourire
aux 750 écoliers qui essaient de nous serrer la main ou d'obtenir de
nous un "hello". Mathieu accroche la mini enceinte a ma capuche et
m'utilise comme alarme ambulante pour les faire fuir.

Nous nous posons dans une petite gastinitsa au prix compétitif de 6
euros la nuit pour deux. Un chien de garde un peu énervé tente de me
foncer dessus, mais se retrouve bien con quand sa chaine se tend et
l'étrangle. Les aboiements font alors place aux couinements ridicules
pendant 2 minutes. Qui fait le malin prend le train comme on dit!
