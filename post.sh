#!/bin/sh

jour=$1

google-drive-ocamlfuse ~/fuse
if [ $? -eq 0 ]; then
    echo "Mounted fuse drive: OK"
else
    echo "Mounted fuse drive: Failed"
    exit 1
fi

if [ $# -eq 1 ]
then
    date_jour=`date +"%Y%m%d"`
else
    date_jour=$(date -d"$2" +"%Y%m%d")
fi

cp ~/fuse/gpx_tracks/$date_jour.gpx ~/site/static/tracks/day$jour.gpx

cd ~/site/static/tracks
./merge.sh

cd ~/site/static/media/photos/
pic_folder=day$jour
mkdir $pic_folder

cd $pic_folder
nautilus . 2> /dev/null &
read -e -p "cover:" cover
cover=$(echo $cover | sed "s/JPG/jpg/g")

cd ~/site/static/media/photos/day$jour
mogrify -quality 50 -format jpg * && rm *.JPG

cd ~/site/static/media/
./convert.sh $jour

cd ~/site/content/post/

date_jour_txt=`date +"%Y-%m-%d"`
date_jour_full=$date_jour_txt"T10:17:43Z"

echo "#+TITLE: Jour $jour 
#+DATE: $date_jour_full
#+DRAFT: false
#+AUTHOR: 
#+COVER: day$jour.jpg
#+PLACE: 
#+SUMMARY: 
#+MAP: true

** Etape 

** Kilomètres

** Météo

** Phrase du jour

** Résumé
" > day$jour.org

cd ~/site/data
echo "[[entries]]
	name = \"Jour $jour\"
	path = \"day$jour\"
	thumb = \"$cover\"
" | cat - gallery.toml > temp && mv temp gallery.toml

cp ~/site/static/media/photos/day$jour/$cover ~/site/static/covers/day$jour.jpg
