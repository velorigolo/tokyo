#!/bin/sh

for d in photos/day$1 ; do
    fgallery -t -d -j 4 $d gallery/`basename $d`
done
