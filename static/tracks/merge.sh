#!/bin/sh

INPUT_FILES=""
for track in `ls day*.gpx | sort -V`
do
    INPUT_FILES="$INPUT_FILES -f $track"
done
    
gpsbabel -i gpx $INPUT_FILES -o gpx -F all.gpx
sed -i '/ele\>/d' all.gpx
sed -i '/time\>/d' all.gpx
sed -i '/speed\>/d' all.gpx
sed -i '/geoidheight\>/d' all.gpx
sed -i '/sat\>/d' all.gpx
sed -i '/hdop\>/d' all.gpx
sed -i '/vdop\>/d' all.gpx
sed -i '/pdop\>/d' all.gpx
sed -i '/course\>/d' all.gpx
sed -i '/src\>/d' all.gpx
